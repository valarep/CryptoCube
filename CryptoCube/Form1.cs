﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CryptoCube.Classes;

namespace CryptoCube
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CubeContainer cubes = new CubeContainer();
            cubes.Crypte("Ceci est un test !");
            Console.WriteLine("crypt : " + cubes);
            Console.WriteLine("Sequence : " + cubes.globalSequence);
            CubeContainer cubes2 = new CubeContainer();
            cubes2.Decrypte(cubes.ToString(), cubes.globalSequence);
            Console.WriteLine("decrypt : " + cubes2);
        }
    }
}
