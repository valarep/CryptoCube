﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoCube.Classes
{
    class Sequence : List<Direction>
    {
        private static Random random;
        public Sequence() : base() { }

        public Sequence(string sequenceString) : this()
        {
            string[] letters = sequenceString.Split(':');
            foreach(string letter in letters)
            {
                if(!Enum.TryParse(letter, out DirectionLetter directionLetter))
                {
                    throw new ArgumentException("Invalid Sequence");
                }
                else
                {
                    Add((Direction)directionLetter);
                }
            }
        }

        public override string ToString()
        {
            string str = "";
            List<DirectionLetter> letters = new List<DirectionLetter>();
            foreach(Direction direction in this)
            {
                DirectionLetter letter = (DirectionLetter)direction;
                letters.Add(letter);
            }
            str = String.Join(":", letters.ToArray());
            return str;
        }

        public static Sequence CreateRandomSequence()
        {
            if (random == null)
            {
                random = new Random(Convert.ToInt32(DateTime.UtcNow.ToBinary() % int.MaxValue));
            }

            Sequence sequence = new Sequence();

            int count = random.Next(5, 7);
            for (int index = 0; index < count; index++)
            {
                int next = random.Next(0, 3);
                Direction direction = (Direction)next;
                sequence.Add(direction);
            }
            return sequence;
        }

        public static List<Sequence> CreateSequenceList(string globalSequence)
        {
            List<Sequence> sequences = new List<Sequence>();
            string[] sequenceArray = globalSequence.Split(',');
            for(int index = 0; index < sequenceArray.Length; index++)
            {
                string cubeSequence = sequenceArray[index];
                int i = int.Parse(cubeSequence.Substring(0, 1));
                if (i != index)
                {
                    throw new ArgumentException("Invalid Sequence format");
                }
                string sequenceString = cubeSequence.Substring(2);
                sequences.Add(new Sequence(sequenceString));
            }
            return sequences;
        }
    }
}
